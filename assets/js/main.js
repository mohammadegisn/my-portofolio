/*=============== SHOW MENU ===============*/
const navMenu = document.getElementById("nav-menu"),
  navToggle = document.getElementById("nav-toggle"),
  navClose = document.getElementById("nav-close");

/*===== MENU SHOW =====*/
/* Validate if constant exists */
if (navToggle) {
  navToggle.addEventListener("click", () => {
    navMenu.classList.add("show-menu");
  });
}

/*===== MENU HIDDEN =====*/
/* Validate if constant exists */
if (navClose) {
  navClose.addEventListener("click", () => {
    navMenu.classList.remove("show-menu");
  });
}

/*=============== REMOVE MENU MOBILE ===============*/
const navLink = document.querySelectorAll(".nav__link");

function linkAction() {
  const navMenu = document.getElementById("nav-menu");
  // When we click on each nav__link, we remove the show-menu class
  navMenu.classList.remove("show-menu");
}
navLink.forEach((n) => n.addEventListener("click", linkAction));

/*=============== CHANGE BACKGROUND HEADER ===============*/
function scrollHeader() {
  const header = document.getElementById("header");
  // When the scroll is greater than 50 viewport height, add the scroll-header class to the header tag
  if (this.scrollY >= 50) header.classList.add("scroll-header");
  else header.classList.remove("scroll-header");
}
window.addEventListener("scroll", scrollHeader);

/*=============== SCROLL SECTIONS ACTIVE LINK ===============*/
const sections = document.querySelectorAll("section[id]");

function scrollActive() {
  const scrollY = window.pageYOffset;

  sections.forEach((current) => {
    const sectionHeight = current.offsetHeight,
      sectionTop = current.offsetTop - 58,
      sectionId = current.getAttribute("id");

    if (scrollY > sectionTop && scrollY <= sectionTop + sectionHeight) {
      document
        .querySelector(".nav__menu a[href*=" + sectionId + "]")
        .classList.add("active-link");
    } else {
      document
        .querySelector(".nav__menu a[href*=" + sectionId + "]")
        .classList.remove("active-link");
    }
  });
}
window.addEventListener("scroll", scrollActive);

/*=============== SHOW SCROLL UP ===============*/
function scrollUp() {
  const scrollUp = document.getElementById("scroll-up");
  // When the scroll is higher than 460 viewport height, add the show-scroll class to the a tag with the scroll-top class
  if (this.scrollY >= 460) scrollUp.classList.add("show-scroll");
  else scrollUp.classList.remove("show-scroll");
}
window.addEventListener("scroll", scrollUp);

/*=============== SCROLL REVEAL ANIMATION ===============*/
const sr = ScrollReveal({
  origin: "top",
  distance: "60px",
  duration: 2500,
  delay: 400,
  // reset: true
});

/*=============== ACCORDION ===============*/
const accordionItems = document.querySelectorAll(".accordion__item");

// 1. Selecionar cada item
accordionItems.forEach((item) => {
  const accordionHeader = item.querySelector(".accordion__header");

  // 2. Seleccionar cada click del header
  accordionHeader.addEventListener("click", () => {
    // 7. Crear la variable
    const openItem = document.querySelector(".accordion-open");

    // 5. Llamar a la funcion toggle item
    toggleItem(item);

    // 8. Validar si existe la clase
    if (openItem && openItem !== item) {
      toggleItem(openItem);
    }
  });
});

// 3. Crear una funcion tipo constante
const toggleItem = (item) => {
  // 3.1 Crear la variable
  const accordionContent = item.querySelector(".accordion__content");

  // 6. Si existe otro elemento que contenga la clase accorion-open que remueva su clase
  if (item.classList.contains("accordion-open")) {
    accordionContent.removeAttribute("style");
    item.classList.remove("accordion-open");
  } else {
    // 4. Agregar el height maximo del content
    accordionContent.style.height = accordionContent.scrollHeight + "px";
    item.classList.add("accordion-open");
  }
};

sr.reveal(`.home-swiper, .new-swiper, .newsletter__container`);
sr.reveal(`.category__data, .trick__content, .footer__content`, {
  interval: 100,
});
sr.reveal(`.about__data, .discount__img`, { origin: "left" });
sr.reveal(`.about__img, .discount__data`, { origin: "right" });
sr.reveal(`.home__title, .home__subtitle`, { origin: "right" });
sr.reveal(`.timeline-content`, { origin: "right" });

document.addEventListener("click", function (e) {
  if (e.target.classList.contains("category__img")) {
    // const src = e.target.getAttribute("src");
    // document.querySelector(".modal-img").src = src;
    const myModal = new bootstrap.Modal(
      document.getElementById("gallery-modal")
    );
    myModal.show();
  }
});

document.addEventListener("click", function (e) {
  if (e.target.classList.contains("category__img2")) {
    const myModal = new bootstrap.Modal(
      document.getElementById("gallery-modal2")
    );
    myModal.show();
  }
});

document.addEventListener("click", function (e) {
  if (e.target.classList.contains("category__img3")) {
    const myModal = new bootstrap.Modal(
      document.getElementById("gallery-modal3")
    );
    myModal.show();
  }
});

document.addEventListener("click", function (e) {
  if (e.target.classList.contains("category__img4")) {
    const myModal = new bootstrap.Modal(
      document.getElementById("gallery-modal4")
    );
    myModal.show();
  }
});

document.addEventListener("click", function (e) {
  if (e.target.classList.contains("category__img5")) {
    const myModal = new bootstrap.Modal(
      document.getElementById("gallery-modal5")
    );
    myModal.show();
  }
});
